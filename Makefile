up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear test-clear docker-pull docker-build docker-up test-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

test-init: test-composer-install test-wait-db test-migrations test-fixtures test-ready

test-clear:
	docker run --rm -v ${PWD}/app:/app --workdir=/app alpine rm -f .ready

test-composer-install:
	docker-compose run --rm test-php composer install

test-wait-db:
	until docker-compose exec -T test-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done

test-migrations:
	docker-compose run --rm test-php php bin/console doctrine:migrations:migrate --no-interaction

test-fixtures:
	docker-compose run --rm test-php php bin/console doctrine:fixtures:load --no-interaction

test-ready:
	docker run --rm -v ${PWD}/app:/app --workdir=/app alpine touch .ready