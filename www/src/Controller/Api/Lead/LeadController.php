<?php

namespace App\Controller\Api\Lead;

use App\Controller\Api\PaginationSerializer;
use App\ReadModel\Lead\LeadFetcher;
use App\ReadModel\Lead\LeadFilter;
use OpenApi\Annotations as OA;
use App\Model\Lead\UseCase\Create\CreateCommand;
use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/leads", name="leads")
 */
class LeadController extends AbstractController
{

    private const PER_PAGE = 10;

    private $serializer;
    private $denormalizer;
    private $validator;

    public function __construct(SerializerInterface $serializer, DenormalizerInterface $denormalizer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
        $this->validator = $validator;
    }

    /**
     * @OA\Get(
     *     path="/leads",
     *     tags={"Leads"},
     *     @OA\Parameter(
     *         name="filter[status]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="string"),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="filter[created_by]",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="integer"),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success response",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="items", type="array", @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="id", type="string"),
     *                 @OA\Property(property="name", type="string"),
     *                 @OA\Property(property="source_id", type="integer"),
     *                 @OA\Property(property="status", type="string"),
     *                 @OA\Property(property="created_at", type="date"),
     *                 @OA\Property(property="created_by", type="integer"),
     *             )),
     *             @OA\Property(property="pagination", ref="#/components/schemas/Pagination"),
     *         )
     *     )
     * )
     * @Route("", name="", methods={"GET"})
     * @param Request $request
     * @param LeadFetcher $fetcher
     * @return Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function index(Request $request, LeadFetcher $leadFetcher): Response
    {
        $filter = LeadFilter::all();

        $filter = $this->denormalizer->denormalize($request->query->get('filter', []), LeadFilter::class, 'array', [
            'object_to_populate' => $filter
        ]);

        $pagination = $leadFetcher->all(
            $filter,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort'),
            $request->query->get('direction')
        );

        return $this->json([
            'items' => array_map(static function (array $item) {
                return [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'source_id' => $item['source_id'],
                    'status' => $item['status'],
                    'created_at' => $item['created_at'],
                    'created_by' => $item['created_by']
                ];
            }, (array)$pagination->getItems()),
            'pagination' => PaginationSerializer::toArray($pagination),
       ]);
    }

    /**
     * @OA\Post(
     *     path="/leads/create",
     *     tags={"Create Lead"},
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *             type="object",
     *             required={"name", "source_id", "status", "created_by"},
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="source_id", type="integer"),
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="created_by", type="integer"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success response",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Errors",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     ),
     * )
     * @Route("/create", name=".create", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request, CommandBus $commandBus): Response
    {
        $command = $this->serializer->deserialize($request->getContent(), CreateCommand::class, 'json');

        $violations = $this->validator->validate($command);
        if (\count($violations)) {
            $json = $this->serializer->serialize($violations, 'json');
            return new JsonResponse($json, 400, [], true);
        }

        $commandBus->handle($command);

        return $this->json([]);
    }
}