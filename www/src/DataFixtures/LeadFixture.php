<?php

namespace App\DataFixtures;

use App\Model\Lead\Entity\Lead;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class LeadFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $date = new \DateTimeImmutable('-100 days');

        $statuses = [
            'active',
            'inactive'
        ];

        for ($i = 0; $i < 20; $i++) {

            $status = $faker->randomElement($statuses);
            $lead = $this->createRandomLead($faker, $status, $date);
            $date = $date->modify('+' . $faker->numberBetween(1, 3) . 'days 5minutes');

            $manager->persist($lead);
        }

        $manager->flush();
    }

    private function createRandomLead(Generator $faker, string $status, \DateTimeImmutable $date): Lead
    {
        return new Lead(
            $faker->sentence(random_int(2, 3)),
            $faker->numberBetween(1, 4),
            $status,
            $date,
            $faker->numberBetween(1, 4)
        );
    }
}
