<?php

namespace App\Model\Lead\Entity;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lead|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lead|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lead[]    findAll()
 * @method Lead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadRepository extends ServiceEntityRepository
{
    private $em;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lead::class);

        $this->em = $this->getEntityManager();
    }

    public function add(Lead $lead): void
    {
        $this->em->persist($lead);
    }

    public function remove(Lead $lead): void
    {
        $this->em->remove($lead);
    }
}
