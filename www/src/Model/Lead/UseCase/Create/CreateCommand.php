<?php

declare(strict_types=1);

namespace App\Model\Lead\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

class CreateCommand
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $source_id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $status;
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $created_by;
    /**
     * @var \DateTimeImmutable
     */
    public $created_at;

    public function __construct(string $name, int $source_id, string $status, int $created_by)
    {
        $this->name = $name;
        $this->source_id = $source_id;
        $this->status = $status;
        $this->created_at = new \DateTimeImmutable('now');
        $this->created_by = $created_by;
    }
}
