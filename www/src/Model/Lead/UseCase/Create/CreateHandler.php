<?php

namespace App\Model\Lead\UseCase\Create;

use App\Model\Flusher;
use App\Model\Lead\Entity\Lead;
use App\Model\Lead\Entity\LeadRepository;

class CreateHandler
{
    private $leadRepository;
    private $flusher;

    public function __construct(LeadRepository $leadRepository, Flusher $flusher)
    {
        $this->leadRepository = $leadRepository;
        $this->flusher = $flusher;
    }

    public function handle(CreateCommand $command): void
    {
        $lead = new Lead(
            $command->name,
            $command->source_id,
            $command->status,
            $command->created_at,
            $command->created_by
        );

        $this->leadRepository->add($lead);

        $this->flusher->flush();
    }
}
