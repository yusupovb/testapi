<?php

declare(strict_types=1);

namespace App\ReadModel\Lead;

use App\Model\Lead\Entity\Lead;
use App\Model\Work\Entity\Projects\Task\Task;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class LeadFetcher
{
    private $connection;
    private $paginator;
    private $repository;

    public function __construct(Connection $connection, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(Lead::class);
    }

    public function find(string $id): object
    {
        return $this->repository->find($id);
    }

    /**
     * @param LeadFilter $filter
     * @param int $page
     * @param int $size
     * @param string $sort
     * @param string $direction
     * @return PaginationInterface
     */
    public function all(LeadFilter $filter, int $page, int $size, ?string $sort, ?string $direction): PaginationInterface
    {
        if (!\in_array($sort, [null, 'name', 'source_id', 'status', 'created_at', 'created_by'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb = $this->connection->createQueryBuilder()->from('leads')->select('*');

        if ($filter->created_by) {
            $qb->andWhere('created_by = :created_by');
            $qb->setParameter(':created_by', $filter->created_by);
        }

        if ($filter->status) {
            $qb->andWhere('status = :status');
            $qb->setParameter(':status', $filter->status);
        }

        if (!$sort) {
            $sort = 'id';
            $direction = $direction ?: 'desc';
        } else {
            $direction = $direction ?: 'asc';
        }

        $qb->orderBy($sort, $direction);

        $pagination = $this->paginator->paginate($qb, $page, $size);

        $leads = (array)$pagination->getItems();

        $pagination->setItems(array_map(static function (array $lead) {
            return $lead;
        }, $leads));

        return $pagination;
    }
}
