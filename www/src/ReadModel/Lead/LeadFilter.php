<?php

namespace App\ReadModel\Lead;

class LeadFilter
{
    public $created_by;
    public $status;

    public static function all(): self
    {
        return new self(null);
    }
}
