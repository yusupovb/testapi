<?php

namespace App\Tests\Functional\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LeadTest extends WebTestCase
{
    private $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = static::createClient();

        $this->client->setServerParameters([
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW' => '123456',
        ]);
    }

    public function testGet(): void
    {

        $this->client->request('GET', '/api/leads');

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertEquals([
            'name' => 'JSON API',
        ], $data);
    }
}

