<?php

namespace App\Tests\Unit\Model\Lead\Entity;

use App\Model\Lead\Entity\Lead;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    public function testSuccess(): void
    {
        $lead = new Lead(
            $name = 'test',
            $source_id = 1,
            $status = 'active',
            $created_at = new \DateTimeImmutable(),
            $created_by = 1
        );

        self::assertEquals($name, $lead->getName());
        self::assertEquals($source_id, $lead->getSourceId());
        self::assertEquals($status, $lead->getStatus());
        self::assertEquals($created_at, $lead->getCreatedAt());
        self::assertEquals($created_by, $lead->getCreatedBy());
    }
}
